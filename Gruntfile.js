var systemPath = require('path');
var blocksFolder = 'frontend/components';

module.exports = function(grunt) {

    // Load Grunt tasks declared in the package.json file
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    var webpack = require("webpack");
    var webpackConfig = require("./webpack.config.js");

    function getTemplate (tech) {
        return grunt.file.read(systemPath.join('frontend/factory', tech + '.txt'));
    }
    function toCamelCase (str) {
        return str.split('-').reduce(function (result, item, index) {
            var str = item[0].toUpperCase() + item.slice(1, item.length);

            return result + str;
        }, '');
    }
    function createTechFile(blockName, folderPath, tech) {
        for (j = 0; j < tech.length; j++) {
            switch (tech[j]) {
                case 'jade':
                    tpl = getTemplate('jade');
                    tpl = tpl.replace(/{{blockname}}/g, blockName);
                    break;
                case 'styl':
                    tpl = getTemplate('styl');
                    tpl = tpl.replace(/{{blockname}}/g, blockName);
                    break;
                case 'js':
                    tpl = getTemplate('js', blockName);
                    tpl = tpl.replace(/{{blockname}}/g, toCamelCase(blockName));
                    break;
                default:
                    tpl = '';
            }

            grunt.file.write(folderPath + "/" + blockName + "." + tech[j], tpl);
        };
    }

    // Configure Grunt
    grunt.initConfig({
        webpack: {
            options: webpackConfig,
            dev: {
                devtool: 'source-map'
            }
        },
        "webpack-dev-server": {
            options: {
                webpack: webpackConfig
            },
            start: {
                contentBase: 'app',
                keepAlive: true
            }
        },
        stylus: {
            dev: {
                files: [
                    {'app/css/main.css': "frontend/**/*.styl"}
                ]
            },
        },
        svg_sprite: {
            basic: {
                // Target basics
                expand: true,
                cwd: 'frontend/resources/svg_to_combine',
                src: ['*.svg'],
                dest: 'frontend/styles',
                // Target options
                options             : {
                    mode            : {
                        css         : {     // Activate the «css» mode
                            render  : {
                                styl : true  // Activate CSS output (with default options)
                            },
                            sprite : '../../app/images/sprite.svg',
                            dest: ''

                        }
                    },
                    shape               : {
                        spacing         : {         // Add padding
                            padding     : 0
                        }

                    }
                }
            }
        },
        sprite:{
            all: {
                src: 'frontend/resources/png_to_combine/*.png',
                dest: 'app/images/sprite.png',
                destCss: 'frontend/styles/png-sprites.styl',
                cssVarMap: function (sprite) {
                    sprite.name = 'icon-' + sprite.name;
                },
                algorithm: 'top-down',
                imgPath: '../images/sprite.png'
            }

        },
        'string-replace': {
            all: {
                files: [{
                    expand: true,
                    cwd: 'app/css',
                    src: ['*.css'],
                    dest: 'app/css'
                }],
                options: {
                    replacements: [{
                        pattern: /\.\.\/\.\.\/app\/images/ig,
                        replacement: '../images'
                    }]
                }
            }
        },
        watch: {
            options: {
                livereload: true,
                spawn: false,
                interrupt: true
            },
            app: {
                files: ["app/**/*"]
            },
            stylus: {
                files: ['frontend/**/*.styl'],
                tasks: ['stylus_compile'],
                livereload: false
            },
            js: {
                files: ["frontend/!**!/!*.js"],
                tasks: ['webpack'],
                livereload: false
            },
        },
        connect: {
            server: {
                options: {
                    port: 9001,
                    base: 'app',
                    livereload: true
                }
            }
        }
    });

    // Загружаем задачи
    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-svg-sprite');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-spritesmith');


    // Компиляция Stylus
    grunt.registerTask('stylus_compile', ['stylus:dev', 'string-replace']);

    // Регистрируем задачи
    //grunt.registerTask("default", ["webpack-dev-server:start", "watch"]);

    grunt.registerTask("default", ["connect", "webpack", "watch"]);
    grunt.registerTask('svg_combine', ['svg_sprite']);
    grunt.registerTask('png_combine', ['sprite']);

    /* ТАСК ДЛЯ СОЗДАНИЯ ФАЙЛОВ БЛОКА */
    grunt.registerTask('create', function () {
        var name = grunt.option('name') ? grunt.option('name').split(',') : [],
            tech = grunt.option('tech') ? grunt.option('tech').split(',') : ['js', 'styl'],
            blockTypeFolder = grunt.option('type') === 'b' ? 'base' : 'complex',
            blockFolder, // папка для конкретного блока
            i;

        // создаваемый блок обязан иметь имя
        if (!name.length) {
            grunt.log.error("Введите имя блока");
            return;
        }

        for (i = 0; i < name.length; i++) {
            blockFolder = systemPath.normalize(systemPath.join(blocksFolder, blockTypeFolder, name[i]));
            // blockFolder = path.normalize(path.join(blocksFolder, name[i]));

            // Не создаем блок если он уже существует
            if (grunt.file.exists(blockFolder)) {
                grunt.log.error("Блок с именем «" + name[i] + "» уже существует");
                continue;
            }

            // Всегда создаем папку для картинок. Автоматически создатся и папка блока
            grunt.file.mkdir(systemPath.normalize(systemPath.join(blockFolder)));
            // создаем файлы для каждой технологии
            createTechFile(name[i], blockFolder, tech);
            grunt.log.success("Блок «" + name[i] + "» успешно создан");
        }
    });
};