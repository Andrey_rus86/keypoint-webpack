'use strict'
// Подключение загруженных модулей через npm
const webpack = require('webpack');
// Модуль по работе с путями
const path = require('path');
// Переменная окружения (по умолчанию development)
const NODE_ENV = process.env.NODE_ENV || 'development';
// Плагин для вытаскивания произвольного текста из файла модуля
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
    // Контекст указывает базовую директорию для поиска описываемых модулей (файлов)
    context: __dirname + '/frontend',
    // Точка входа описывает файл, подключаемый на первичном этапе загрузки приложения
    entry: {
        app: ['webpack-dev-server/client', './js/main.js'],
        vendor: ["react", "react-dom", "react-router"],
    },
    output: {
        path: path.resolve(__dirname, 'app/'),
        chunkFilename: 'js/webpack_chunks/[name].js',
        filename: 'js/build.js'
    },

    // Опция watch указывает системе следить за изменением файлов и осуществлять liveReload в браузере.
    watch: NODE_ENV == 'development',

    watchOptions: {
        // Опция aggregateTimeout устанавливает задержку в мс для пересборки файлов. Полезно для оптимизации.
        aggregateTimeout: 300,
        poll: true

    },

    progress: true,
    keepalive: false,

    // Генерация sourcemap файлов включена
    devtool: 'source-map',

    module: {
        // Лоадеры - это преобразователи (трансформаторы) для определенного набора файлов.
        loaders: [
            {
                /*
                    Babel-loader использует модуль babel для перобразования современного синтаксиса javascript ES6
                    в кроссбраузерный javascript.

                    Здесь же используется preset babel для компиляции JSX синтаксиса.
                 */
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            // лоадеры срабатывают справа налево, разделяются восклицательным знаком. Опции указываются полсе вопросительного знача.
/*            {
                test: /\.styl$/,
                exclude: /node_modules/,
                loader: ExtractTextPlugin.extract('css-loader!autoprefixer-loader?browsers=last 2 versions!stylus-loader?resolve url')
            }*/
        ]
    },

    // Плагины позволяют вносить изменения на различных этапах для сборки в целом
    plugins: [],

    devServer: {
        contentBase: "app"
    }

};

// Плагины
if(NODE_ENV == 'production') {
    module.exports.plugins.push(
        // Плагин минификации файлов проекта
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    );
}

module.exports.plugins.push(
    // Плагин разбиения модуля на более мелкие составляющие пакеты (файлы)
    // chunkName, fileName
    new webpack.optimize.CommonsChunkPlugin({ name: 'vendor', filename: 'plugins/vendor.bundle.js' })
);

/*module.exports.plugins.push(
    new ExtractTextPlugin("css/main.css", {allChunks: true})
);*/


