import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Link, hashHistory} from 'react-router';
import rootRoute from '../routes/routes';


// Router: прикрепляем компонент роутера в корневой элемент
ReactDOM.render(
    <Router history={hashHistory} routes={rootRoute} />,
    document.getElementById('root')
);