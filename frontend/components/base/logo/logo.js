import React, {Component as ReactComponent} from 'react';

class Logo extends ReactComponent {
    render() {
        return (
            <div className="logo">Logo component</div>
        );
    }
};

export {Logo};
