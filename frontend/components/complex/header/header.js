import React, {Component as ReactComponent} from 'react';
import {Logo} from '../../base/logo/logo';

class Header extends ReactComponent {
    render() {
        return (
            <header className="header" >
                <div className="basicContainer">
                    <div className="header__logoCont">
                        <Logo />
                    </div>
                </div>
            </header>
        );
    }
};

export {Header};