import React, {Component as ReactComponent} from 'react';
import {Router, Route, Link, hashHistory} from 'react-router';
import {Header} from '../header/header';

// Корневй компонент приложения
class App extends ReactComponent {
    render() {
        return (
            <div>
                <Header />
                <main id="content" className="basicContainer">
                    {this.props.children}
                </main>
                <footer>
                    <Link to="/about">About</Link>
                </footer>
            </div>
        );
    }
}

export {App};