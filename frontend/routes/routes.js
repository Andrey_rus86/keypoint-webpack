import {App} from '../components/complex/app/app';

export default {
    childRoutes: [ {
        path: '/',
        component: App,
        childRoutes: [
            require('./about'),
        ]
    } ]
};